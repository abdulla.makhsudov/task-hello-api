package az.ingress.simplyhelloapp.service;

import az.ingress.simplyhelloapp.entity.Counter;
import az.ingress.simplyhelloapp.repository.HelloRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HelloService {

    private final HelloRepository helloRepository;

    public String getCounter() {
        Counter counter = helloRepository.findById(1L).orElseThrow(() -> new RuntimeException("Counter not found"));
        Counter increasedCounter = updateCounter(counter);
        return "Hello, the counter is : " + increasedCounter.getCounter();
    }

    public Counter updateCounter(Counter counter) {
        counter.setCounter(counter.getCounter() + 1);
        return helloRepository.save(counter);
    }
}
