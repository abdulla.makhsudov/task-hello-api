package az.ingress.simplyhelloapp;

import az.ingress.simplyhelloapp.entity.Counter;
import az.ingress.simplyhelloapp.repository.HelloRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class SimplyHelloAppApplication implements CommandLineRunner {

    private final HelloRepository helloRepository;

    public static void main(String[] args) {
        SpringApplication.run(SimplyHelloAppApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Counter counter = Counter.builder()
                .counter(0)
                .build();
        helloRepository.save(counter);
    }
}
